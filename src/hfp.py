from cog_worker import Worker
import rasterio as rio
import numpy as np
import mahotas
import os
import logging
from scipy import ndimage
from .config import *

from src.utils import stac_search, upload_obj_to_az


def fast_distance_transform(arr):
    if arr.ndim == 3 and arr.shape[0] == 1:
        arr = arr[0]
        return mahotas.distance(arr)[np.newaxis]
    return mahotas.distance(arr)


def HFP(
    Shard: Worker, 
    year = 2020,
    cgls = False,
    az_output_container = None,
    az_output_prefix = '',
):

    def get_nav_rivers():
        assets = stac_search(STAC_API_URL, COLLECTIONS['rivers_nav'], bbox=Shard.lnglat_bounds(buffered=True))
        return Shard.read(assets)

    def get_lakes():
        assets = stac_search(STAC_API_URL, COLLECTIONS['lakes'], bbox=Shard.lnglat_bounds(buffered=True))
        land = Shard.read(assets)
        return np.ma.array(land, mask=land==0)

    def get_land():
        assets = stac_search(STAC_API_URL, COLLECTIONS['land'], bbox=Shard.lnglat_bounds(buffered=True))
        land = Shard.read(assets)
        return np.ma.array(land, mask=land==0)

    def get_populated_areas(pop_buffer=4000, year=DEFAULT_YEAR):
        year = np.clip(year, MIN_YEAR, MAX_YEAR)
        buffer_px = pop_buffer//Shard.scale
        assets = stac_search(STAC_API_URL, COLLECTIONS['nightlights'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
        ntl = Shard.read(assets)
        populated_areas = ntl > 0
        populated_areas_buffer = fast_distance_transform(~populated_areas) <= buffer_px**2
        return populated_areas_buffer

    def get_roads():
        assets = stac_search(STAC_API_URL, COLLECTIONS['roads'], bbox=Shard.lnglat_bounds(buffered=True))
        return Shard.read(assets)

    def get_rail():
        assets = stac_search(STAC_API_URL, COLLECTIONS['railways'], bbox=Shard.lnglat_bounds(buffered=True))
        return Shard.read(assets)

    def get_population(year=DEFAULT_YEAR, pop_max=POP_MAX):
        year = np.clip(year, MIN_YEAR, MAX_YEAR)
        kernel_size = round(1000 / Shard.scale)
        pop_max_100m = pop_max / 100
        
        assets = stac_search(STAC_API_URL, COLLECTIONS['population'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
        pop = Shard.read(assets)
        pop[pop.mask] = 0
        pop = ndimage.uniform_filter(pop[0], kernel_size)[np.newaxis]
        pop = np.clip(np.log(pop+1)/np.log(pop_max_100m), 0, 1)
        return pop

    def get_nightlights(year=DEFAULT_YEAR, ntl_max=NTL_MAX):
        year = np.clip(year, MIN_YEAR, MAX_YEAR)
        assets = stac_search(STAC_API_URL, COLLECTIONS['nightlights'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
        if len(assets):
            ntl = Shard.read(assets[0])
            ntl = np.clip(np.log(ntl+1)/np.log(ntl_max), 0, 1)
        else:
            ntl = Shard.empty()
        return ntl
        
    def get_builtareas(year=DEFAULT_YEAR, cgls=False):
        if cgls:
            year = np.clip(year, MIN_YEAR, 2019)
            assets = stac_search(STAC_API_URL, COLLECTIONS['cgls-lc100'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
            landcover = Shard.read(assets)
            return landcover == 50
        else:
            assets = stac_search(STAC_API_URL, COLLECTIONS['landcover'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
            landcover = Shard.read(assets)
            return landcover == 7

    def get_croplands(year=DEFAULT_YEAR, cgls=False):
        if cgls:
            year = np.clip(year, MIN_YEAR, 2019)
            assets = stac_search(STAC_API_URL, COLLECTIONS['cgls-lc100'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
            landcover = Shard.read(assets)
            return landcover == 40
        else:
            assets = stac_search(STAC_API_URL, COLLECTIONS['landcover'], f'{year}-01-01/{year}-12-31', bbox=Shard.lnglat_bounds(buffered=True))
            landcover = Shard.read(assets)
            return landcover == 5

    def get_pastures():
        pastures = Shard.read(COLLECTIONS['pastures'])
        pastures[np.isnan(pastures)] = 0
        return pastures

    def calc_shorelines(land, lakes=None):
        '''
        Returns the edge of all land 
        (Land pixels directly adjacent to water)
        
        0 = Land
        1 = Shoreline
        Null = Water
        '''
        water = land.mask
        if lakes is not None:
            water = np.logical_or(water, lakes)
        shoreline = fast_distance_transform(np.logical_not(water)) == 1
        return np.ma.array(shoreline, mask=water)

    def calc_nav_waters(
        shorelines,
        nav_rivers,
        populated_areas,
        max_nav_distance=MAX_NAV_DISTANCE,
        scale=Shard.scale
    ):
        '''
        Find shorelines that could be reached from potential ports.
        
        Filters rivers and shorelines to areas within
        `max_distance` of shorelines in populated areas.
        
        If `cost_distance` use A* to navigate distances along 
        shorelines, otherwise use euclidian (as-the-crow-flies) distance.
        '''
        
        max_distance_px = max_nav_distance // scale

        waters = np.logical_or(shorelines, nav_rivers)
        populated_waters = np.logical_and(waters, populated_areas)
        
        populated_waters_dist = fast_distance_transform(np.logical_not(populated_waters))
        nav_waters = np.logical_and((populated_waters_dist <= (max_distance_px**2)), waters)
        
        return nav_waters

    def _buffer_and_decay(
        arr,
        buffer=0, 
        decay_rate=0.001, 
        decay_multiplier=1
    ):
        '''
        Buffers and/or creates an exponential decay around non-zero pixels
        
        `buffer` distance in meters from pixel center to buffer
        `decay_rate` distance scaler for exponential decay
            0.001 -> divide by Math.E every 1000m
            0.0006931 -> divide by 2 every 1000m
        `decay_multiplier` scaler for exponential decay pixels relative to buffered pixels
        '''
        scale = Shard.scale
        
        buffer = max(0, buffer - scale/2) # remove half a pixel from the buffer distance
        max_dist_px = (4 / decay_rate + buffer) // scale # max distance decays to e^(-4) ~= 0.02
        distance = np.sqrt(fast_distance_transform(np.logical_not(arr))) * scale
        max_dist = distance > max_dist_px * scale

        if decay_rate and decay_multiplier:
            out = np.where(distance <= buffer, 1, np.exp((buffer-distance) * decay_rate) * decay_multiplier)
            return np.ma.array(out, mask=max_dist)
        out = distance <= buffer
        return np.ma.array(out, mask=~out)

    def get_hfp_layers(year=DEFAULT_YEAR, cgls=False):
        land = get_land()
        lakes = get_lakes()
        shorelines = calc_shorelines(land, lakes)
        populated_areas = get_populated_areas(year)
        nav_rivers = get_nav_rivers()
        nav_waters = calc_nav_waters(shorelines, nav_rivers, populated_areas)
        # 1.2 Roads
        roads = get_roads()
        # 1.3 Railways
        rail = get_rail()
        # 1.4 Normalized population density
        population = get_population(year)
        # 1.5 Normalized nighttime lights
        nightlights = get_nightlights(year)
        # 1.6 Land cover classes
        builtareas = get_builtareas(year, cgls)
        crops = get_croplands(year, cgls)
        pastures = get_pastures()

        layers = {
            # access layers
            'roads': roads,
            'rail': rail,
            'nav_waters': nav_waters,

            # human presence layers
            'population': population,
            'nightlights': nightlights,
            
            # land cover layers
            'builtareas': builtareas,
            'crops': crops,
            'pastures': pastures,
            
            'mask': land.mask
        }
        return layers

    def calc_hfp(
        layers=None,
        weights=WEIGHTS,
        groups=DEFAULT_LAYER_GROUPS,
        decay_settings=DECAY_SETTINGS,
        year=DEFAULT_YEAR,
        cgls=False,
        mask=False
    ):
        """
        Generate Human Footprint with given weight/decay parameters
        
        Various intermediate layers will be cached following `export_opts`.
        Optionally, cache the resulting HFP image to `cache_hfp_asset`.
        
        layers          dictionary of input layers or None to use default
        weights         dictionary of layer weights for HFP index
        groups          list of lists of layers to flatten together (e.g. land cover layers)
        decay_settings  dictionary of options to buffer or decay layers
        """
        
        # 0. Load/derive input layers
        layers = get_hfp_layers(year, cgls) if layers is None else layers.copy()
        weights = weights.copy()
        mask = mask | layers.pop('mask')
        if layers.keys() != weights.keys():
            raise Exception(f"Layers and weights do not match. Found {layers.keys()-{'mask'}}, {set(weights.keys())}")

        # 1. Buffer/decay access layers (roads, rail, nav_waters)
        logging.info('calculating buffers')
        for name, arr in layers.items():
            if name in decay_settings:
                logging.info(name)
                logging.info(arr.sum())
                layers[name] = _buffer_and_decay(
                    arr,
                    **decay_settings[name]
                )
                
        # 2. Multiply layers by weights
        for name, arr in layers.items():
            layers[name] = arr * weights[name]

        # 3. Flatten grouped layers (land cover) into single layer
        #     Where different layers in the same group overlap, 
        #     take the highest value layer.
        weight_sum = 0
        for group in groups:
            flattened = np.sum([layers.pop(name) for name in group], axis=0)
            layers[group] = flattened
            weight_sum += max([weights.pop(name) for name in group])
        for name in weights.keys():
            weight_sum += weights[name]
        
        # 4. Sum layers, mask
        hfp = np.sum(list(layers.values()), axis=0)
        hfp = np.ma.array(hfp)
        if mask is not None:
            hfp.mask = hfp.mask | mask
        
        return hfp

    # 

    fname = f"hfp_{year}_{round(Shard.scale)}m_{Shard.bounds[3]}_{Shard.bounds[0]}_cog.tif"
    logging.info(f'Calculating {fname}')
    arr = calc_hfp(year=year, cgls=cgls)
    logging.info(f'Completed {fname}')

    if az_output_container:
        with rio.MemoryFile() as bytes:
            Shard.write(arr, bytes, driver='COG')
            bytes.seek(0)
            blobname = os.path.join(az_output_prefix, fname)
            logging.info(f'Uploading {fname}')
            upload_obj_to_az(bytes, az_output_container, blobname)
    else:
        logging.warning('`az_output_container` not specified, discarding results')

    return arr


