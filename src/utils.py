
import os
from cog_worker.types import BoundingBox
import satstac
import requests
from datetime import datetime
from azure.storage.blob import ContainerClient
import numpy as np
import logging


def stac_search(
    url: str,
    collection_id: str,
    date_period: str = None,
    items: list = None,
    geom: dict = None,
    bbox: BoundingBox = None,
    query: dict = {},
    fields: list = [],
    page_limit: int = 500
):

    headers = {
        'Content-Type': 'application/json'
    }
    if query:
        for key, sub_dict in query.items():
            for k, v in sub_dict.items():
                if isinstance(v, str):
                    query[key][k] = f'"{v}"'
    logging.info(bbox)
    payload = {
        'collections': [collection_id],
        'ids': items,
        'intersects': geom,
        'bbox': bbox if bbox is not None and all(np.isfinite(bbox)) else None,
        'query': query,
        'limit': page_limit,
        "fields": {
            "include": [f"properties.{f}" for f in fields]
        }
    }
    if date_period:
        dates = date_period.split('/')
        start_datetime = datetime.fromisoformat(dates[0]).strftime("%Y-%m-%dT%H:%M:%SZ")
        end_datetime = datetime.fromisoformat(dates[1]).strftime("%Y-%m-%dT%H:%M:%SZ")
        date_period = f"{start_datetime}/{end_datetime}"
        payload['datetime'] = date_period
    r = requests.post(f'{url}/search', json=payload, headers=headers)
    resp = r.json()
    features = list()
    while resp.get("features"):
        feats = resp.pop('features')
        features.extend(feats)
        for link in resp['links']:
            if link['rel'] == 'next':
                payload["token"] = link["body"]["token"]
                r = requests.post(f"{link['href']}", json=payload, headers=headers)
                resp = r.json()

    items = [satstac.Item(i) for i in features]
    logging.info((collection_id, len(items)))
    return [asset['href'] for item in items for key, asset in item.assets.items()]



def upload_obj_to_az(fileobj, container, blobname):
    AZURE_STORAGE_ACCOUNT = os.getenv('AZURE_STORAGE_ACCOUNT')
    AZURE_SAS = os.getenv('AZURE_SAS')
    AZURE_STORAGE_CONNECTION_STRING = os.getenv('AZURE_STORAGE_CONNECTION_STRING')

    if AZURE_STORAGE_ACCOUNT and AZURE_SAS:
        azclient = ContainerClient.from_container_url(f'https://{AZURE_STORAGE_ACCOUNT}.blob.core.windows.net/{container}?{AZURE_SAS}')
    elif AZURE_STORAGE_CONNECTION_STRING:
        azclient = ContainerClient.from_connection_string(AZURE_STORAGE_CONNECTION_STRING, container)
    else:
        raise Exception('Must set AZURE_STORAGE_ACCOUNT and AZURE_SAS or AZURE_STORAGE_CONNECTION_STRING')
    
    azclient.upload_blob(blobname, fileobj, overwrite=True)