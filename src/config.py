# constants
ASSETS = {
    'landcover':    'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/2013_meris_lc.tif',
    'coastlines':   'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/coastlines.tif',
    'rivers_nav':   'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/rivers_nav.tif',
    'population':   'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/gpwv4_popden_2013.tif',
    'nightlights':  'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/elvidge2013_dmsp.tif',
    'roads':        'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/roads_osm_groads_union.tif',
    'railways':     'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/railways.tif',
    'pastures':     'https://storage.googleapis.com/francis_indicators/backup/HFP/HFP_source/pastures2000_filled.tif'
}

STAC_API_URL = "https://io-stac-internal.azurewebsites.net"

COLLECTIONS = {
    'nightlights': 'io-vizz-dynamic-science-VNL-v2',
    'rivers_nav': 'io-vizz-dynamic-science-HydroRIVERS-v10-2m-100m',
    'lakes': 'io-vizz-dynamic-science-hydrolakes-polys-v10-100km3m-2m-100m',
    'railways': 'io-vizz-dynamic-science-osm-rails-100m-20210422',
    'roads': 'io-vizz-dynamic-science-osm-roads-100m-20210204',
    'population': 'io-vizz-dynamic-science-worldpop',
    'land': 'io-vizz-dynamic-science-land-mask',
    'landcover': 'esri-2020-land-cover-10m',
    'pastures' : 'https://impactobservatory.blob.core.windows.net/io-vizz-dynamic-science/pastures2000/pastures2000_filled.tif',
    'cgls-lc100': 'io-vizz-dynamic-science-CGLS-LC100',
}

SCALE = 1000

DEFAULT_YEAR = 2019
MAX_YEAR = 2020
MIN_YEAR = 2015

NAV_DEPTH = 2 # m
MAX_NAV_DISTANCE = 80000 # m
LARGE_LAKE_DEPTH = 3 # m
LARGE_LAKE_AREA = 100 # km2
NTL_MAX = 25 # µW/cm2/sr
POP_MAX = 1000 # pop/km2
POPULATED_AREAS_THRESH = 1 # µW/cm2/sr
POPULATED_AREAS_DIST = 1000 # m

# `weights`
# These define the relative weight of each layer
WEIGHTS = {
    'nav_waters': 4,
    'roads': 8,
    'rail': 8,
    'population': 10,
    'nightlights': 10,
    'builtareas': 10,
    'crops': 6,
    'pastures': 1
}

# `groups`
# These define which layers should be flattened together to be treated as single layer
DEFAULT_LAYER_GROUPS = [
    ('builtareas', 'crops', 'pastures')
]

# `decay_settings`
# These define which layers need to be buffered or have a decay function
#  they are passed to the `_buffer_and_decay_cached_dist` function
DECAY_SETTINGS = {
    'roads': {
        'buffer':500, # meters
        'decay_multiplier':0.5, # multiplier for decay value vs buffer value
        'decay_rate': 0.001 # exponential decay rate in meters
    },
    'rail': {
        'buffer':500,
        'decay_multiplier':0,
        'decay_rate': 0.001
    },
    'nav_waters':{
        'buffer':0,
        'decay_multiplier':1,
        'decay_rate': 0.001
    }
}
