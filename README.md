# Human footprint script

Calculate Global Terrestrial Human Footprint

## Introduction

The scripts in this repository were used to produce the 100m global terrestrial Human Footprint for 2015-2020.

This repository was prepared for the publication of: 
 Gassert et. al. "An operational approach to near real time global high resolution mapping of the terrestrial Human Footprint." Manuscript. Science Advances.

These scripts allow for computation of Human Footprint at arbitrary scale and extent. For example the full global analysis can be previewed at a greatly reduced scale using the `python main.py preview` command. Large scale computation is enabled by relying on prepared data sources stored in Cloud Optimized Geotiff (COG) formats, and indexed in Spatial Temporal Asset Catalog (STAC). Utilities are provided for chunking analysis in tiles or "shards", as well as for executing the analysis in the Microsoft Planetary Computer PanGeo infrastructure.

Computation relies on several hundred gigabytes of input data sources. All source data are open access data sources, however some prepreprocessing has been preformed to simplify analysis. Access to preprocessed source datasets stored in Impact Observatory Azure cloud storage may be provided upon request.

Data sources, weights, and normalization parameters can be configured in the `src/config.py` file. Additional utility scripts are included as python notebooks in the `notebooks` directory.

## Entrypoint

`./main.py` is a simple python cli with three options:
 - `preview` preview analysis
 - `execute` execute analysis reading json parameters from stdin
 - `iterjsons` generate json parameters for a chucked analysis
 - `./main.py {command} --help` print help

e.g. Preview global analysis (computes a 1024px global version)
```
python main.py preview --verbose
```

e.g. Preview chunked analysis at 20km scale
```
python main.py iterjsons --scale 20000 --shardsize=512 --buffer=20 | python main.py execute --verbose --show-plot
```

e.g. Execute complete analysis over  uploading the results to az://io-vizz-dynamic-science/hfp-test/
```
python main.py iterjsons --az-output-container=io-vizz-dynamic-science --az-output-blob-prefix=hfp-test | python main.py execute
```

### Azure credentials

In order to read source data as well as upload analysis results to Azure blob storage, you must provide environment variables:
 - `AZURE_STORAGE_ACCOUNT` and `AZURE_SAS`
 - or `AZURE_STORAGE_CONNECTION_STRING`

### Saving analysis results

In order to save analysis results you must provide a blob storage container and an optional prefix:
 - env `AZ_OUTPUT_CONTAINER` or option `--az-output-container`
 - env `AZ_OUTPUT_BLOB_PREFIX` or option `--az-output-blob-prefix`
