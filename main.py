#!/usr/bin/env python

import json
import click
import logging

from cog_worker import Manager
from src.hfp import HFP
import numpy as np

@click.group()
def main():
    """Run HFP script"""
    pass


@main.command('preview')
@click.option('--verbose', flag_value=True)
@click.option('--year', type=int, default=2020, help="Year of analysis (2015-2020)")
@click.option('--cgls', flag_value=True, help="Use CGLS-LC100 instead of IO landcover")
def preview(
    verbose: bool,
    year: int,
    cgls: bool,
):
    '''preview analysis'''
    from rasterio import plot

    if verbose:
        logging.basicConfig(level=logging.INFO)

    manager = Manager(proj='+proj=moll', scale=100)

    arr, bbox = manager.preview(HFP, f_kwargs={'year':year, 'cgls':cgls})
    from rasterio import plot
    plot.show(arr)
    

@main.command('execute')
@click.argument('line', type=str)
@click.option('--verbose', flag_value=True)
@click.option('--show-plot', flag_value=True)
def execute(line, verbose = False, show_plot = False):
    """Read json parameters from stdin and execute analysis"""

    if verbose:
        logging.basicConfig(level=logging.INFO)

    manager = Manager()
    params = json.loads(line)
    
    arr, bbox = manager.execute(HFP, **params)
    if show_plot:
        from rasterio import plot
        plot.show(arr)


@main.command('iterjsons')
@click.option('--proj', type=str, default="+proj=moll", help="Output projection for analysis")
@click.option('--scale', type=float, default=100, help="Scale in projection units (usu meters)")
@click.option('--buffer', type=int, default=400, help="Buffer pixels around bounding box to avoid edge effects")
@click.option('--year', type=int, default=2020, help="Year of analysis (2015-2020)")
@click.option('--cgls', flag_value=True, help="Use CGLS-LC100 instead of IO landcover")
@click.option('--shardsize', type=int, default=4096, help="Block size to chunk analysis")
@click.option('--az-output-container', type=str, envvar="AZ_OUTPUT_CONTAINER")
@click.option('--az-output-blob-prefix', type=str, envvar="AZ_OUTPUT_BLOB_PREFIX")
def chunk_params(
    proj: str, 
    scale: float,
    buffer: int,
    shardsize: int,
    year: int,
    cgls: bool,
    az_output_container: str = None,
    az_output_blob_prefix: str = None,
):
    '''Echo json parameter objects for running in batch'''
    manager = Manager(proj=proj, scale=scale, buffer=buffer)
    for obj in manager.chunk_params(chunksize=shardsize):
        obj['f_kwargs'] = {
            'cgls': cgls,
            'year': year
        }
        if az_output_container:
            obj['f_kwargs']['az_output_container'] = az_output_container
        if az_output_blob_prefix:
            obj['f_kwargs']['az_output_prefix'] = az_output_blob_prefix
        bounds = manager.proj.transform_bounds(*obj['proj_bounds'], densify_pts=shardsize+buffer-1, direction='inverse')
        if all(np.isfinite(bounds)):
            click.echo(json.dumps(obj))


if __name__ == '__main__':
    main()